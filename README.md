# Vim Configuration Files
==================================================================
## Juan Felipe Restrepo. 
## jf.restrepo.rinckoar@bioingenieria.edu.ar.
==================================================================

## Setup

1. Clone Repository: 
    * git clone https://jrinckoar@bitbucket.org/jrinckoar/dotvim.git ~/.vim

2. Create symlinks: 
    * ln -s ~/.vim/vimrc ~/.vimrc

3. Init plugins:
    * Run cd ~/.vim
    * Run git submodule init
    * Run git submodule update

-----------------------------------------------------------

## Install plugins

1. Run git submodule add http://github.com/x-plugin.git bundle/x-plugin
2. add "ignore=dirty" in .gitmodules :
3. Run git add .
4. Run git commit -m "Install x-pluging bundle as a submodule."

-----------------------------------------------------------

## Remove plugins

1. Delete the relevant section from the .gitmodules file.
2. Run git add .gitmodules.
3. Delete the relevant section from .git/config.
4. Run git rm --cached path_to_submodule (no trailing slash).
5. Run rm -rf .git/modules/submodule_name.
6. Commit
7. Run rm -rf path_to_submodule.

-----------------------------------------------------------
        
## Update to the latest version of each plugin bundle
1. git pull origin master
2. git submodule foreach git pull origin master

-----------------------------------------------------------

## Need to be installed

* lynx
* python-psutils
* curl
* wget

-----------------------------------------------------------

## SyncTex

* install vim-gtk okular zenity gawk
* Okular command :gvim --servername GVIM --remote +%l %f




