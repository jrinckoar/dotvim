" Latex Config file
"=============================================================================
"=============================================================================
setlocal formatoptions=1 
setlocal tw=120
setlocal ts=8
setlocal sts=1
setlocal sw=1
setlocal iskeyword+=\\
setlocal makeprg=make
setlocal keywordprg=:help
"setlocal noexpandtab 
set complete+=s
set formatprg=par
setlocal wrap 
setlocal linebreak
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
"Set a left margin
highlight! link FoldColumn Normal
setlocal foldcolumn=5 
"=============================================================================
"=============================================================================
" Thesaurus
"noremap <buffer> <F12> :OnlineThesaurusCurrentWord<CR> 
"=============================================================================
"=============================================================================
" ATP
set iskeyword+=:
let g:Tex_Leader = '#'
let g:Tex_DefaultTargetFormat = 'pdf'
let b:atp_Viewer = 'okular'
let g:Tex_CompileRule_pdf = 'pdflatex -interaction=nonstopmode --enable-write18 $*'
let b:atp_TexCompiler = "pdflatex"
let b:atp_TexFlavor = "latex"
let b:atp_ProjectScript = 0
let g:Tex_AutoFolding = 0
let g:Tex_Folding = 0
set winaltkeys=no

noremap <buffer> <leader>kk :Tex<CR>
noremap <buffer> <leader>ll :2Tex<CR>
noremap <buffer> <leader>bb :Bibtex<CR>
noremap <buffer> <leader>aa :Tex<CR>:Bibtex<CR>:2Tex<CR>
noremap <buffer> <leader>vv :View<CR>
noremap <buffer> <leader>ss :ShowErrors<CR>
noremap <buffer> <leader>df :Delete<CR>
noremap <buffer> <leader>la :Labels<CR>

























